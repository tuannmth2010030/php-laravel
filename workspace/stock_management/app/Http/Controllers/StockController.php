<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stock;
class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()//view default
    {
        //2 buoc :+, step call instance of model -> step 2
        // redirect to view ->step 3
        $stocks = Stock::latest()->paginate(5);//step2
        return view('stocks.index',compact('stocks'))->with('i',(request()->input('page',1)-1)*5);//step3
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //add new item in model(stock)
        return view('stocks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//hien thi toan bo thong tin
    {
        //save data to database
        $request->validate([
            'pro_name'=>'required',
            'pro_desc'=>'required',
            'qty'=>'required',
            'image'=>'required',
        ]);

        //step 2 call model
        Stock::create($request->all());

        //step 3
        return redirect()->route('stocks.index')->with('success','created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        return view('stocks.show',compact('stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('stock.edit',compact('stock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
        $request->validate([
            'pro_name'=>'required',
            'pro_desc'=>'required',
            'qty'=>'required',
            'image'=>'required',
        ]);
        $stock->update($request->all());
        return redirect()->route('stock.index')->with('success','Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
        $stock->delete();
    }
}
