@extends('stocks.layout')
@section('content')

<div class="text-center">
    <h2>Product management</h2>
</div>
<div>
    <div class="mb-3 text-center">
        <a href="{{ route('stocks.create') }}" class="btn btn-success">Add stock</a>
    </div>
</div>
@if ($message=Session::get('success'))
<div class="alert alert-success">
    {{$message}}
</div>
    
@endif
<table class="table table-bordered table-warning">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Description</th>
        <th>Qty</th>
        <th>Picture</th>
        <th>id</th>
        <th width="150px">...</th>
    </tr>
    @foreach ($stocks as $stock)
        <tr>
            <td>{{ ++$i}}</td>
            <td>{{ $stock->pro_name}}</td>
            <td>{{ $stock->pro_desc}}</td>
            <td>{{ $stock->qty}}</td>
            <td>{{ $stock->image}}</td>
            <td>{{$stock->id}}</td>
            <a href="{{$stock->id}}" class="btn btn-primary">Edit</a>
        </tr>
    @endforeach
</table>