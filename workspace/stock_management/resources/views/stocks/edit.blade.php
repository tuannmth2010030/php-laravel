@extends('stocks.layout')
@section('content')

<h1 class="text-center">Add new stock</h1>
<form action="{{route('stocks.update',$stock->id)}}" method="POST">
    @csrf
    @method('PUT');
<div class="form-group">
    <strong>Product:</strong>
    <input type="text" name="pro_name" value="{{$stock->pro_name}}" class="form-control" placeholder="Product">
</div>
<div class="form-group">
    <strong>Description:</strong>
    <input type="text" name="pro_desc" value="{{$stock->pro_desc}}" class="form-control" placeholder="Description">
</div>
<div class="form-group">
    <strong>Quantity:</strong>
    <input type="text" name="qty" value="{{$stock->qty}}" class="form-control" placeholder="Quantity">
</div>
<div class="form-group">
    <strong>Picture:</strong>
    <input type="text" name="image" value="{{$stock->image}}" class="form-control" placeholder="Picture">
</div>

<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
