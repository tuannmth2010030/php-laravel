<?php

namespace Database\Seeders;

use App\Models\Stock;
use Illuminate\Database\Seeder;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Stock::create([

            'pro_name' => 'iphone',
            'pro_desc' => 'The new product of Apple',
            'qty' =>10,
            'image' => 'apple.jpg'
        ]);

    }
}
