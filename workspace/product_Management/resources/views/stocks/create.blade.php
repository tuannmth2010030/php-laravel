@extends('stocks.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="h2 text-center">Add Stock</div>
        </div>
        <div class="col-lg-12 text-center" style="margin-top: 10px;">
           <a href="{{route('stocks.index')}}" class="btn btn-primary">Back</a>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Oops!</strong>There were some problems with your input. <br><br>
        <ul>
            @foreach ($errors->all() as $$error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
<form action="{{route('stocks.store')}}" method="POST">
    @csrf
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Name: </strong>
                <input type="text" name="product_name" class="form-control" placeholder="Product Name">
            </div>
            <div class="form-group">
                <strong>Product Description</strong>
                <input type="text" name="product_desc" placeholder="Product Description" class="form-control">
            </div>
            <div class="form-group">
                <strong>Qty.</strong>
                <input type="text" name="product_qty" placeholder="Quantity" class="form-control">
            </div>
            <div class="form-group">
                <strong>Picture</strong>
                <input type="text" name="product_image" placeholder="Picture" class="form-control">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </div>
    </div>
</form>
@endsection